const express = require('express')
const router = express.Router()
const userController = require('../controller/user')
const register = require('../controller/register/app')
const registerHR = require('../controller/register/hr')

router.post('/login/', userController.login)

router.post('/logout/', userController.logout)

router.post('/registerApp/', register)

router.post('/registerHR/', registerHR)

module.exports = router
