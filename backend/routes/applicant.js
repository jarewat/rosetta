const express = require('express')
const router = express.Router()
const applicantController = require('../controller/ApplicantController')

/* GET users listing. */
router.get('/', applicantController.getApplicants)

router.get('/:id', applicantController.getApplicants)

router.post('/', applicantController.addApplicant)
router.put('/', applicantController.updateApplicantAnm)

router.put('/', applicantController.updateApplicant)

router.delete('/:id', applicantController.deleteApplicant)

module.exports = router
