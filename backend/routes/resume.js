const express = require('express')
const router = express.Router()
const resumeController = require('../controller/ResumeController')

/* GET users listing. */
router.get('/', resumeController.getResumes)

router.get('/:id', resumeController.getResume)

router.post('/', resumeController.addResume)

router.put('/', resumeController.updateResume)

router.delete('/:id', resumeController.deleteResume)

module.exports = router
