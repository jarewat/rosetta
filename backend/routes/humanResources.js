const express = require('express')
const router = express.Router()
const humanResourcesController = require('../controller/HumanResourcesController')

/* GET users listing. */
router.get('/', humanResourcesController.getHumanResourcess)

router.get('/:id', humanResourcesController.getHumanResources)

router.post('/', humanResourcesController.addHumanResources)

router.put('/', humanResourcesController.updateHumanResources)

router.delete('/:id', humanResourcesController.deletehumanHumanResources)

module.exports = router
