const express = require('express')
const router = express.Router()
const jobApplyController = require('../controller/JobApplyController')

/* GET users listing. */
router.get('/', jobApplyController.getJobApplications)

router.get('/:id', jobApplyController.getJobApplication)

router.post('/', jobApplyController.addJobApplication)

router.put('/', jobApplyController.updateJobApplication)

router.delete('/:id', jobApplyController.deleteJobApplication)

module.exports = router
