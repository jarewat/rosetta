const express = require('express')
const router = express.Router()
const addressController = require('../controller/AddressController')

/* GET users listing. */
router.get('/', addressController.getAddresses)

router.get('/:id', addressController.getAddress)

router.post('/', addressController.addAddress)

router.put('/', addressController.updateAddress)

router.delete('/:id', addressController.deleteAddress)

module.exports = router
