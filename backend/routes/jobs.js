const express = require('express')
const router = express.Router()
const jobController = require('../controller/JobsController')

router.get('/', jobController.getJobs)

router.get('/:id', jobController.getJob)

router.post('/', jobController.addJob)

router.put('/', jobController.updateJob)

router.delete('/:id', jobController.deleteJob)

module.exports = router
