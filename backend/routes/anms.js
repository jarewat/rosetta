const express = require('express')
const router = express.Router()
const announcementController = require('../controller/AnnouncementController')

/* GET users listing. */
router.get('/', announcementController.getAnms)

router.get('/:id', announcementController.getAnm)

router.get('/search/:position', announcementController.searchByText)

router.post('/', announcementController.addAnm)

router.put('/', announcementController.updateAnm)

router.delete('/:id', announcementController.deleteAnm)

module.exports = router
