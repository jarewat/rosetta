const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const cors = require('cors')
const mongoose = require('mongoose')
const session = require('express-session')

mongoose.connect('mongodb://admin:password@localhost/jobfinder', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})
const db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function () {
  console.log('mongodb JobFinder connected')
})

const indexRouter = require('./routes/index')
const usersRouter = require('./routes/users')
const anmsRouter = require('./routes/anms')
const jobsRouter = require('./routes/jobs')
const addressRouter = require('./routes/address')
const applicantRouter = require('./routes/applicant')
const companyRouter = require('./routes/company')
const humanResourcesRouter = require('./routes/humanResources')
const resumeRouter = require('./routes/resume')
const jobApplyRouter = require('./routes/jobapply')

const app = express()

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(cors())
app.use(session({
  secret: 'secretKey',
  resave: true,
  saveUninitialized: true,
  cookie: { secure: true }
}))

app.use('/', indexRouter)
app.use('/users', usersRouter)
app.use('/anms', anmsRouter)
app.use('/jobs', jobsRouter)
app.use('/address', addressRouter)
app.use('/applicant', applicantRouter)
app.use('/company', companyRouter)
app.use('/humanResources', humanResourcesRouter)
app.use('/resume', resumeRouter)
app.use('/jobapply', jobApplyRouter)

module.exports = app
