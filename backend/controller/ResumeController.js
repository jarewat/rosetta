const Resume = require('../models/Resume')

const ResumeController = {
  addResume (req, res, next) {
    const payload = req.body
    const resume = new Resume(payload)
    resume.save()
      .then(() => {
        res.json(resume)
      })
      .catch(err => {
        res.status(500).send(err)
      })
  },
  async updateResume (req, res, next) {
  },
  deleteResume (req, res, next) {
  },
  getResumes (req, res, next) {
    Resume.find({})
      .exec(function (err, resumes) {
        if (err) {
          console.log(err)
        }
        res.json({ resumes })
      })
  },
  getResume (req, res, next) {
  }
}

module.exports = ResumeController
