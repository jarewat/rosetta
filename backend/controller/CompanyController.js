const Company = require('../models/Company')

const companyController = {
  addCompany (req, res, next) {
    const payload = req.body
    const company = new Company(payload)
    company.save()
      .then(() => {
        res.json(company)
      })
      .catch(err => {
        res.status(500).send(err)
      })
  },
  async updateCompany (req, res, next) {
  },
  deleteCompany (req, res, next) {
  },
  getCompanys (req, res, next) {
    Company.find({})
      .populate('address')
      .exec(function (err, companys) {
        if (err) {
          console.log(err)
        }
        res.json({ companys })
      })
  },
  getCompany (req, res, next) {
    const { id } = req.params
    Company.findById(id, {})
    .populate('address')
      .exec(function (err, company) {
        if (err) {
          console.log(err)
        }
        res.json({ company })
      })
  }
  
}

module.exports = companyController
