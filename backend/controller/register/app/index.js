const Resume = require('../../../models/Resume')
const Address = require('../../../models/Address')
const Applicant = require('../../../models/Applicant')

const register = function (req, res, next) {
  const resumePayload = req.body.resume
  const addressPayload = req.body.address
  const applicantPayload = req.body.applicant

  let resumeID = ''
  let addressID = ''

  const address = new Address(addressPayload)
  address.save()
    .then((add) => {
      const resume = new Resume(resumePayload)
      addressID = add._id
      resume.save()
        .then((resu) => {
          resumeID = resu._id
          const applicant = new Applicant(applicantPayload)
          applicant.resume = resumeID
          applicant.address = addressID
          applicant.save()
            .then((app) => {
              res.json(app)
            })
            .catch(err => {
              res.status(500).send(err)
            })
        })
        .catch(err => {
          res.status(500).send(err)
        })
    })
    .catch(err => {
      res.status(500).send(err)
    })
}

module.exports = register
