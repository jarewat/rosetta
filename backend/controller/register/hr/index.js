const Company = require('../../../models/Company')
const HumanResources = require('../../../models/HumanResources')

const registerHR = function (req, res, next) {
  const companyPayload = req.body.company
  const hrPayload = req.body.humanResources

  let companyId = ''

  const company = new Company(companyPayload)
  company.save()
    .then((com) => {
      companyId = com._id
      const hr = HumanResources(hrPayload)
      hr.company = companyId
      hr.save()
        .then((result) => {
          res.json(result)
        }).catch(err => {
          res.status(500).send(err)
        })
    })
    .catch(err => {
      res.status(500).send(err)
    })
}

module.exports = registerHR
