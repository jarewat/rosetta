const HumanResources = require('../models/HumanResources')

const humanResourcesController = {
  addHumanResources (req, res, next) {
    const payload = req.body
    const humanResources = new HumanResources(payload)
    humanResources
      .save()
      .then(() => {
        res.json(humanResources)
      })
      .catch(err => {
        res.status(500).send(err)
      })
  },
  async updateHumanResources (req, res, next) {},
  deletehumanHumanResources (req, res, next) {},
  getHumanResourcess (req, res, next) {
    HumanResources.find({})
      .populate('app_announce')
      .populate('company')
      .exec(function (err, humanResourcess) {
        if (err) {
          console.log(err)
        }
        res.json({ humanResourcess })
      })
  },
  getHumanResources (req, res, next) {
    const { id } = req.params
    HumanResources.findById(id, {})
      .populate('app_announce')
      .populate('company')
      .exec(function (err, humanResourcess) {
        if (err) {
          console.log(err)
        }
        res.json({ humanResourcess })
      })
  }
}

module.exports = humanResourcesController
