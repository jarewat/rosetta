const Job = require('../models/Job')

const jobController = {
  addJob (req, res, next) {
    const payload = req.body
    const job = new Job(payload)
    job.save()
      .then(() => {
        res.json(job)
      })
      .catch(err => {
        res.status(500).send(err)
      })
  },
  async updateJob (req, res, next) {
  },
  deleteJob (req, res, next) {
  },
  getJobs (req, res, next) {
    Job.find({})
      .exec(function (err, jobs) {
        if (err) {
          console.log(err)
        }
        res.json({ jobs })
      })
  },
  getJob (req, res, next) {
  }
}

module.exports = jobController
