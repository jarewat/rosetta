const Address = require('../models/Address')

const addressController = {
  addAddress (req, res, next) {
    const payload = req.body
    const address = new Address(payload)
    address.save()
      .then(() => {
        res.json(address)
      })
      .catch(err => {
        res.status(500).send(err)
      })
  },
  async updateAddress (req, res, next) {
  },
  deleteAddress (req, res, next) {
  },
  getAddresses (req, res, next) {
    Address.find({})
      .exec(function (err, address) {
        if (err) {
          console.log(err)
        }
        res.json({ address })
      })
  },
  getAddress (req, res, next) {
  }
}

module.exports = addressController
