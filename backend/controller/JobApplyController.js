const JobApplication = require('../models/JobApplication')
const Applicant = require('../models/Applicant')
const HR = require('../models/HumanResources')
const jobApplyController = {
  addJobApplication (req, res, next) {
    const payload = req.body
    const jobApplication = new JobApplication(payload)
    jobApplication
      .save()
      .then(ja => {
        Applicant.updateOne(
          { _id: ja.applicant },
          { $push: { jobapplication: [ja._id] } }
        )
          .then(result => {
            console.log(result)
          })
          .catch(err => {
            res.status(500).send(err)
          })
        HR.updateOne(
          { app_announce: ja.applicant },
          { $push: { jobapplication: [ja._id] } }
        ).then(result => {
          console.log(result)
          res.status(200).json(jobApplication)
        })
      })
      .catch(err => {
        res.status(500).send(err)
      })
  },
  async updateJobApplication (req, res, next) {},
  deleteJobApplication (req, res, next) {},
  getJobApplications (req, res, next) {
    JobApplication.find({})
      .populate('app_announce')
      .populate('company')
      .exec(function (err, jobApplication) {
        if (err) {
          console.log(err)
        }
        res.json({ jobApplication })
      })
  },
  getJobApplication (req, res, next) {}
}

module.exports = jobApplyController
