const Applicant = require('../../models/Applicant')
const HumanResources = require('../../models/HumanResources')

const login = function (req, res) {
  const email = req.body.email
  const password = req.body.password
  HumanResources.findOne({ email: email, password: password })
    .exec(function (err, humanResourcess) {
      if (err) {
        console.log(err)
      }
      if (humanResourcess === null) {
        Applicant.findOne({ email: email, password: password })
          .exec(function (err, applicants) {
            if (err) {
              console.log(err)
            }
            if (applicants === null) {
              res.status(404).json('user not found')
            } else {
              req.session.user = applicants
              res.status(200).json(req.session.user)
            }
          })
      } else {
        req.session.user = humanResourcess
        res.status(200).json(req.session.user)
      }
    })
}
const logout = function (req, res) {
  req.session.destroy()
  res.status(200).json('loged out')
}

module.exports = {
  login,
  logout
}
