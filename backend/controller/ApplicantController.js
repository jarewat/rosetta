const Applicant = require('../models/Applicant')

const applicantController = {
  addApplicant (req, res, next) {
    const payload = req.body
    const applicant = new Applicant(payload)
    applicant.save()
      .then(() => {
        res.json(applicant)
      })
      .catch(err => {
        res.status(500).send(err)
      })
  },
  async updateApplicantAnm (req, res, next) {
    const payload = req.body
    console.log(payload)
    try {
      const anm = await Applicant.updateOne({ _id: payload._id }, payload.app_announce)
      console.log(anm)
      res.json(anm)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async updateApplicant (req, res, next) {
  },
  deleteApplicant (req, res, next) {
  },
  getApplicants (req, res, next) {
    Applicant.find({})
      .populate('address')
      .populate('resume')
      .populate('app_announce')
      .exec(function (err, applicants) {
        if (err) {
          console.log(err)
        }
        res.json({ applicants })
      })
  },
  getApplicant (req, res, next) {
  }
}

module.exports = applicantController
