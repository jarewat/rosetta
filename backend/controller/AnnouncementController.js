const Announcement = require('../models/Announcement')
const HR = require('../models/HumanResources')

const announcementContrller = {
  addAnm (req, res, next) {
    const payload = req.body
    const anm = new Announcement(payload.anm)
    anm
      .save()
      .then(result => {
        console.log(payload.hr)
        const hrID = payload.hr
        HR.updateOne(
          { _id: hrID },
          { $push: { app_announce: result._id } }
        ).then(result => {
          console.log(result)
          res.json(anm)
        })
      })
      .catch(err => {
        console.log(err)
        res.status(500).send(err)
      })
  },
  async updateAnm (req, res, next) {
    const payload = req.body
    try {
      const anm = await Announcement.updateOne({ _id: payload._id }, payload)
      res.json(anm)
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  },
  deleteAnm (req, res, next) {
    const { id } = req.params
    Announcement.deleteOne({ _id: id })
      .then(() => {
        res.json({ id })
      })
      .catch(err => {
        res.status(500).send(err)
      })
  },
  getAnms (req, res, next) {
    Announcement.find({})
      .populate('job')
      .exec(function (err, anms) {
        if (err) {
          console.log(err)
        }
        res.json({ anms })
      })
  },
  getAnm (req, res, next) {
    const { id } = req.params
    Announcement.findById(id, {})
      .populate('job')
      .exec(function (err, anms) {
        if (err) {
          console.log(err)
        }
        res.json({ anms })
      })
  },
  searchByText (req, res, next) {
    const position = req.params.position
    Announcement.find({})
      .populate('job')
      .exec(function (err, anm) {
        if (err) {
          console.log(err)
          Announcement.findById(id, {})
            .populate('job')
            .exec(function (err, anms) {
              if (err) {
                console.log(err)
              }
              res.status(404).json({ anms })
            })
        }
        const anms = []
        anm.forEach(item => {
          if (
            item.job.position.indexOf(position) >= 0
          ) {
            anms.push(item)
          }
        })
        res.json({ anms })
      })
  }
}

module.exports = announcementContrller
