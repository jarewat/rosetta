const mongoose = require('mongoose')
const Schema = mongoose.Schema

const addressSchema = new Schema({
  village_number: String,
  subdistrict: String,
  district: String,
  province: String,
  zipcode: String
})
module.exports = mongoose.model('Address', addressSchema)
