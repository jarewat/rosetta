const mongoose = require('mongoose')
const Schema = mongoose.Schema

const jobSchema = new Schema({
  position: String,
  career: String
})

module.exports = mongoose.model('Job', jobSchema)
