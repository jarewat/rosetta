const mongoose = require('mongoose')
const Schema = mongoose.Schema

const companySchema = new Schema({
  name: String,
  address: { type: Schema.Types.ObjectId, ref: 'Address' },
})
module.exports = mongoose.model('Company', companySchema)
