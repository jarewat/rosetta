const mongoose = require('mongoose')
const Schema = mongoose.Schema

const announcementSchema = new Schema({
  name: String,
  job: { type: Schema.Types.ObjectId, ref: 'Job' },
  amount: Number,
  address: String,
  min_salary: Number,
  max_salary: Number,
  description: String,
  benefit: String,
  gender: String,
  min_age: Number,
  max_age: Number,
  educate: String,
  min_experience: Number,
  max_experience: Number,
  new_graduates: Boolean,
  properties: String,
  status: Boolean,
  type_job: String,
  date_open: Date,
  date_close: Date,
  skill: String,
})
module.exports = mongoose.model('Announcement', announcementSchema)
