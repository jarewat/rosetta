const mongoose = require('mongoose')
const Schema = mongoose.Schema

const jobApplicationSchema = new Schema({
  app_announce: { type: Schema.Types.ObjectId, ref: 'Announcement' },
  applicant: { type: Schema.Types.ObjectId, ref: 'Applicant' },
  result: String
})
module.exports = mongoose.model('JobApplication', jobApplicationSchema)
