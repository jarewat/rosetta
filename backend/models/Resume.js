const mongoose = require('mongoose')
const Schema = mongoose.Schema

const resumeSchema = new Schema({
  university: String,
  faculty:String,
  branch: String, 
  educational: String,
  gpa: Number,
  languageability: [String],
  ability: String,
  experience: Number
})
module.exports = mongoose.model('Resume', resumeSchema)
