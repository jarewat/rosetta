const mongoose = require('mongoose')
const Schema = mongoose.Schema

const applicantSchema = new Schema({
  email: String,
  name: String,
  surname: String,
  password: String,
  birthday: Date,
  phone: String,
  gender: String,
  marital: String,
  nationality: String,
  religion: String,
  role: String,
  address: { type: Schema.Types.ObjectId, ref: 'Address' },
  resume: { type: Schema.Types.ObjectId, ref: 'Resume' },
  jobapplication: [{ type: Schema.Types.ObjectId, ref: 'JobApplication' }]
})
module.exports = mongoose.model('Applicant', applicantSchema)
