const mongoose = require('mongoose')
const Schema = mongoose.Schema

const humanResourcesSchema = new Schema({
  email: String,
  password: String,
  phone: String,
  name: String,
  surname: String,
  role: String,
  jobapplication: [{ type: Schema.Types.ObjectId, ref: 'JobApplication' }],
  app_announce: [{ type: Schema.Types.ObjectId, ref: 'Announcement' }],
  company: { type: Schema.Types.ObjectId, ref: 'Company' }
})
module.exports = mongoose.model('HumanResources', humanResourcesSchema)
