const Service = {
  acList: [
    {
      id: 1,
      name: 'ภควัฒน์',
      surname: 'คำนึงคุณากร',
      position: 'Senior Developer',
      experience: '2',
      province: 'ลพบุรี',
      education: 'ปริญาตรี',
      salary: 180000,
      birthday: '',
      marital_status: '',
      gender: '',
      nationality: '',
      religion: '',
      house_number: '',
      district: '',
      canton: '',
      postal_code: 15230,
      phone_number: '',
      university: '',
      faculty: '',
      field_of_study: '',
      grade: 2.85,
      language_ability: '',
      charisma: ''
    },
    {
      id: 2,
      name: 'วีรศักดิ์',
      surname: 'เสียงเลิศ',
      position: 'Software Engineer',
      experience: '3',
      province: 'ชลบุรี',
      education: 'ปริญาตรี',
      salary: 22000,
      birthday: '',
      marital_status: '',
      gender: '',
      nationality: '',
      religion: '',
      house_number: '',
      district: '',
      canton: '',
      postal_code: 15230,
      phone_number: '',
      university: '',
      faculty: '',
      field_of_study: '',
      grade: 3.25,
      language_ability: '',
      charisma: ''
    }
  ],
  lastId: 3,
  getApplicants () {
    return [...this.acList]
  }
  // getApplicant (id) {
  //   const index = this.acList.findIndex(item => item.id === 1)
  //   // return [...this.acList[index]]
  //   console.log(id)
  //   console.log(index)
  // }
}

export default Service
