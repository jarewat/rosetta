import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/anms',
    name: 'anms',

    component: () => import('../views/AppAnounments2/')
  },
  {
    path: '/manageApplicant',
    name: 'manageApplicant',

    component: () => import('../views/ManageUser/')
  },
  {
    path: '/showApplicant/:id',
    name: 'showApplicant',

    component: () => import('../views/ManageUser/showdetails.vue')
  },
  {
    path: '/formEdit/:id',
    name: 'formEdit',
    component: () => import('../views/AppAnounments2/formEdit.vue')
  },
  {
    path: '/form',
    name: 'form',
    component: () => import('../views/AppAnounments2/form.vue')
  },
  {
    path: '/userManagementCompany',
    name: 'userManagementCompany',
    component: () => import('../views/userManagementCompany.vue')
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/informationmanagement',
    name: 'informationmanagement',
    component: () => import('../views/User/informationmanagement.vue')
  },
  {
    path: '/statusApplyjob',
    name: 'statusApplyjob',
    component: () => import('../views/StatusApplyJob/index.vue')
  },
  {
    path: '/userRegister',
    name: 'userRegister',
    component: () => import('../views/UserRegister.vue')
  },
  {
    path: '/companyRegister',
    name: 'companyRegister',
    component: () => import('../views/companyRegister.vue')
  },
  {
    path: '/watchForm',
    name: 'watchForm',
    component: () => import('../views/AppAnounments2/watchForm.vue')
  },
  {
    path: '/searchJob',
    name: 'searchJob',
    component: () => import('../views/JobApply/SearchJob.vue')
  },
  {
    path: '/jobDetail',
    name: 'jobDetail',
    component: () => import('../views/User/jobdescription.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
